This file contains a list of common followup questions or information commonly
asked or provided after answers to nm_pp2 questions.

 * PR9. Should you happily sign another developer's OpenPGP key? If not,
        please explain the checks you will make before signing it.

Suppose we meet and I show you my passport and my national ID card, but
I don't have the fingerprint with me, and I tell you that you can just
find my fingerprint in my web page.  Would you sign my key?


 * PRa. Do you know how to create a revocation certificate for your OpenPGP key?
        Do you have one? Why can it be meaningful to set a key expiration date?

What happens if someone's key expires and then they refresh the expiry date
again - is the key valid or invalid? If they get stuck, give the following
hint: The phrase "What happens" is slightly misleading; consider possible
different perspectives of what happened, for example when different people sync
with keyservers at different times, and then consider how to reconcile these
different perspectives into a coherent single model for security reasoning.


 * PRb. What would you do if you wanted to retire from the project?

And what if later on you would like to come back?

There's a shortened NM process for emeritus developers that has been
introduced here: http://lists.debian.org/debian-devel-announce/2005/02/msg00003.html
The idea is not to retest people, just to just ask a few question to be
sure they've caught up with what has happened since they have left.
